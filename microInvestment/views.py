from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.db.models import Count
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_http_methods

from microInvestment.models import Category
from .models import Question, Answer, UserAnswer

TEST_PASSWORD = "testPassword"


# Create your views here.
def index(request):
    """
    Show the home page.
    :param request:
    :return:
    """
    logout(request)
    return render(request, 'microInvestment/index.html')


def poll(request):
    """
    Show the poll page and start it.
    :param request:
    :return:
    """
    logout(request)
    # Check if we need to create a new user
    _check_user(request)
    # Search in the DB for the first question
    question = Question.objects.filter(from_question=None).first()
    # Get its possible answers
    answers = question.answer_set.all()
    # Prepare the context data for the template
    context = {"question": question, "answers": answers}
    # Render the template and return the response
    return render(request, 'microInvestment/poll.html', context)


@require_http_methods(["POST"])
def post_ajax(request):
    """
    We get an AJAX request (is a javascript request without refreshing the browser)
    and we return the segment of the question.
    :param request:
    :return:
    """
    # We work only with AJAX POST request.
    # The user must be authenticated
    if request.is_ajax() and request.user is not None and request.user.is_authenticated:
        # Get the data from the Request object
        answer_id = request.POST["answerID"]
        answer_value = request.POST["answerValue"]
        # Get the possible Answer to the Question
        answer = get_object_or_404(Answer, pk=answer_id)
        # And check that the question was not answered by the user
        already_answered = request.user.useranswer_set.filter(answer__question__id=answer.question_id).count() >= 1
        if not already_answered:
            # Save the User Answer
            UserAnswer.objects.create(answer=answer, user=request.user, text=answer_value)
            # and look for the next question in the DB
            question = answer.next_question
            last_question = question.answer_set.all().count() == 0
            # If we ask the last "Question" (Goodbye), then we log the user out.
            if last_question:
                logout(request)
            # Special case where we need to replace the value that we give to the user
            if "{" in question.text:
                question.text = question.text.format(Value=answer.category.value)
            # Get the possible answer to the Question
            answers = question.answer_set.all()
            # Render the Template snippet with the data context
            data = render(request, template_name='microInvestment/question_section.html', context={
                'question': question, 'answers': answers,
            })
            # and return the response to the browser
            return HttpResponse(data)
    raise Http404("Error")


def show_user_answers(request, user_id):
    """
    Show the answers of the user with ID user_id
    :param request:
    :param user_id:
    :return:
    """
    user = get_object_or_404(User, pk=user_id)
    user_answers = user.useranswer_set.all().order_by("pk")
    context = {"user": user, "answers": user_answers}
    return render(request, 'microInvestment/answers.html', context)


def show_poll_users(request):
    """
    Show the user that have finished the Poll with a link to see their answers.
    :param request:
    :return:
    """
    users = [user for user in User.objects.filter(username__startswith="poll_user_") if _check_user_finished(user)]
    context = {"users": users, }
    return render(request, 'microInvestment/users.html', context)


def _check_user(request):
    """
    Check that a User is authenticated.
    When not, create a new user and authenticated it
    :param request:
    :return:
    """
    if not request.user.is_authenticated:
        poll_users = User.objects.filter(username__startswith="poll_user_")
        if poll_users.count() > 0:
            last_poll_user = User.objects.filter(username__startswith="poll_user_").last()
            user_name = "poll_user_" + str(int(last_poll_user.username.split("poll_user_")[1]) + 1)
        else:
            user_name = "poll_user_1"
        user = User.objects.create_user(user_name, password=TEST_PASSWORD)
        # user = authenticate(username=user_name, password=TEST_PASSWORD)
        login(request, user)
    else:
        user = request.user
    return user


def _check_user_finished(user):
    """
    Return True if the user finished the Poll
    :param user:
    :return:
    """
    if not isinstance(user, User):
        user = get_object_or_404(User, pk=user)
    user_answers = user.useranswer_set.all()
    if user_answers.count() > 0:
        answer = user_answers.last().answer
        return answer.next_question in Question.objects.filter(answer=None)
    return False


def show_categories(request):
    """
    Show a brief summary of the categories
    :param request:
    :return:
    """
    categories = Category.objects.all().annotate(num_users=Count("answer__useranswer"))
    context = {"categories": categories}
    return render(request, 'microInvestment/categories.html', context)


def show_category(request, category_id):
    """
    Show the users that belong to a certain category
    :param request:
    :param category_id:
    :return:
    """
    category = get_object_or_404(Category, pk=category_id)
    users = User.objects.filter(useranswer__answer__category=category_id)
    user_answers = [get_poll_user_data(user) for user in users if _check_user_finished(user)]
    context = {"category": category, "answers": user_answers, "users": users}
    return render(request, 'microInvestment/category.html', context)


def _all_users_that_gave_back():
    a = Question.objects.filter(text__contains="How much").first().answer_set.all().first()
    return [x.user for x in a.useranswer_set.all() if "poll" in x.user.username]


def get_poll_user_data(user):
    if not isinstance(user, User):
        user = get_object_or_404(User, pk=user)
    if _check_user_finished(user):
        user_answers = user.useranswer_set.all()
        if user_answers.filter(answer__question__text__contains="How much").first():
            amount = user_answers.filter(answer__question__text__contains="How much").first().text
        else:
            amount = "No"
        if user_answers.filter(answer__question__text__contains="gender").first():
            sex = user_answers.filter(answer__question__text__contains="gender").first().text
        else:
            sex = "Blank"
        if user_answers.filter(answer__question__text__contains="old").first():
            age = user_answers.filter(answer__question__text__contains="old").first().text
        else:
            age = "Blank"
        if user_answers.filter(answer__question__text__contains="feel satisfied").first():
            satisfied = user_answers.filter(answer__question__text__contains="feel satisfied").first().text
        else:
            satisfied = "Blank"
        if user_answers.filter(answer__question__text__contains="feedback").first():
            feedback = user_answers.filter(answer__question__text__contains="feedback").first().text
        else:
            feedback = "Blank"
        if user_answers.filter(answer__category__isnull=False).first():
            category = user_answers.filter(answer__category__isnull=False).first().answer.category.name
        else:
            category = "Blank"
        return {"user": user.username,
                "sex": sex,
                "age": age,
                "category": category,
                "satisfied": satisfied,
                "feedback": feedback,
                "amount": amount
                }
    return {"user": "",
            "sex": "",
            "age": "",
            "category": "",
            "satisfied": "",
            "feedback": "",
            "amount": ""
            }
