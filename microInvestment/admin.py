from django.contrib import admin

# Register your models here.
from microInvestment.models import Question, Answer, Category, UserAnswer


class AnswerInLine(admin.StackedInline):
    model = Answer
    extra = 1
    fk_name = "question"


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    inlines = (AnswerInLine,)


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('text', 'question', 'next_question')


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')


@admin.register(UserAnswer)
class UserAnswerAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'answer')
