from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Question(models.Model):
    text = models.CharField(max_length=250)

    def __str__(self):
        return self.text


class Category(models.Model):
    name = models.CharField(max_length=100)
    value = models.FloatField()

    def __str__(self):
        return self.name


class Answer(models.Model):
    text = models.CharField(max_length=250, blank=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    next_question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='from_question')
    category = models.OneToOneField(Category, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f'{self.text} - {self.question}'


class UserAnswer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    text = models.CharField(max_length=250)

    def __str__(self):
        return f'{self.user} - {self.text}'
