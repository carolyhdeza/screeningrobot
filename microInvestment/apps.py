from django.apps import AppConfig


# Define the name of the application and create the config object to install it
# in Django settings.
class MicroinvestmentConfig(AppConfig):
    name = 'microInvestment'
