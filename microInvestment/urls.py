"""screeningRobot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name="home"),
    path('poll', views.poll, name="poll"),
    path('ajax/answer', views.post_ajax, name="ajax-answer"),
    path('poll_users', views.show_poll_users, name="show-users"),
    path('poll_users/<user_id>', views.show_user_answers, name="show-user"),
    path('categories', views.show_categories, name="show-categories"),
    path('categories/<category_id>', views.show_category, name="show-category"),
]
