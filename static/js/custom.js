function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


$.fn.extend({
    // Add a new function to jquery for the animation of the "chat"
    // Animate.css is the CSS Library that we use for animation.
    // They recommend this method of detectin when the animation has ended.
    animateCss: function (animationName, callback) {
        var animationEnd = (function (el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function () {
            $(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') callback();
        });

        return this;
    },
});

function processAnswer(htmlID = "", answerID = 0, val = false) {
    /*
    With this function we send the Answer selected by the user to the server
    and we receive the new Question to ask using AJAX Technology
    */
    let element = $("#poll-log");
    let button = $("#" + htmlID);
    let csrftoken = getCookie('csrftoken');
    let value = val;
    if (value === false) {
        value = button.val();
    }
    let data = {
        htmlID: htmlID,
        answerID: answerID,
        answerValue: value,
    };
    console.log(data);
    //Ajax setup to use with Django CSRF Protection.
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    //Here we make the Ajax request
    let request = $.ajax({
        url: 'ajax/answer',
        method: "POST",
        data: data,
        dataType: 'html',
        success: function (data) {
            button.closest(".row").animateCss('fadeOutDown fast', function () {
                button.closest(".row").hide();
            });
            element.prepend(data);
        }
    });
    request.done(function (msg) {
        console.log(msg);
    });
    request.fail(function (jqXHR, textStatus) {
        console.log("Request failed: " + textStatus);
    });
}
